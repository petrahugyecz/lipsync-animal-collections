# LipSync - animal collections

[![LipSync - animal collections](https://img.youtube.com/vi/v8sGzq6oebQ/0.jpg)](https://www.youtube.com/watch?v=v8sGzq6oebQ)

This is an automated 2D mouth animation package with 63 different characters. Every character has it’s own sprites for every english letter, and common collocations (such as ’ee’, ’ch’, ’sh’, ’th’…). The only thing you have to do is add the input text, and that’s all. You can edit the animation speed and you can try it in the editor through the Inspector window, or you can pass the data through code as well. <br> <br> 

<strong>&#11088;  But that's not all ... &#11088;</strong><br> There is a smart text correction system, which you can use to add your own letters from another language and use your own sprites. You can use it to tweak the animation to make it more sightful. <br> <br>

&#10071;<strong> These are sprite animations! &#10071; </strong><br> <br>

<strong> Package contains: </strong><br>
- 63 different characters (each with 11-17 sprites - 560x...x1000 pixels - with transparent background, so you can easily edit them if you want to)<br>
- 1 demo scene and 63 prefabs <br>
- detailed and well commented script
<br> <br>

<strong>Unity Asset Store</strong><br>
https://assetstore.unity.com/packages/slug/203156

<strong>Youtube preview</strong><br>
https://www.youtube.com/watch?v=v8sGzq6oebQ

If you have any questions, suggestions or feedback, please feel free to leave a review or contact me at <a href="mailto:petrahugyecz@gmail.com"> petrahugyecz@gmail.com </a> &#9996;
